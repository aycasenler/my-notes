package com.example.notesapp

import android.app.AlertDialog
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase
import kotlinx.android.synthetic.main.custom_dialog.view.*

class AddNewNoteFragment : Fragment(), View.OnClickListener {

    private var edtTitle: EditText? = null
    private var edtDetail: EditText? = null
    private var edtSummary: EditText? = null
    private var edtKey: EditText? = null
    private var btnCancelAddNote: Button? = null
    private var btnAddNote: Button? = null

    lateinit var mAuth: FirebaseAuth

    lateinit var noteList: MutableList<Unotes>

    lateinit var ref: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.add_notes_fragment, container, false)
        initView(view)

        return view

    }

    fun initView(view: View) {
        noteList = ArrayList()
        edtTitle = view.findViewById(R.id.edtTitle)
        edtDetail = view.findViewById(R.id.edtDetail)
        edtSummary = view.findViewById(R.id.edtSummary)
        edtKey = view.findViewById(R.id.edtKey)
        btnAddNote = view.findViewById(R.id.btnAddNote)
        btnCancelAddNote = view.findViewById(R.id.btnCancelAddNote)

        mAuth = FirebaseAuth.getInstance()
        ref = FirebaseDatabase.getInstance().getReference("Notes")

        btnAddNote?.setOnClickListener(this)
        btnCancelAddNote?.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnCancelAddNote?.id -> {

                val mDialogView =
                    LayoutInflater.from(context).inflate(R.layout.custom_dialog, null)
                val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mDialogView.dialog_txt?.text = "Are you sure ? Your typing will not be saved."
                mDialogView.ok_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                    activity!!.supportFragmentManager.popBackStack()
                }
                mDialogView.cancel_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                }
            }
            btnAddNote?.id -> {
                saveData()
                Handler().postDelayed({
                    activity!!.supportFragmentManager.popBackStack()
                }, 2000)

            }
        }
    }

    fun saveData() {
        val title = edtTitle?.text.toString().trim()
        if (title.isEmpty()) {
            edtTitle?.error = "Please enter note title! "
            return
        }
        val detail = edtDetail?.text.toString().trim()
        if (detail.isEmpty()) {
            edtDetail?.error = "Please enter note detail! "
            return
        }
        var userId : String?  = null
        if (mAuth.currentUser !== null) {
             userId = mAuth.currentUser?.uid
        }
        val keys = edtKey?.text.toString()
        val summary = edtSummary?.text.toString().trim()
        val noteId = ref.push().key
        val notes = Unotes(noteId!!, userId!!, title, detail, keys, summary)

        ref.child(noteId).setValue(notes).addOnCompleteListener {
            Toast.makeText(context, "Note add successfully!", Toast.LENGTH_SHORT).show()
        }

    }

    data class Unotes(
        var noteId: String ="",
        var userId: String = "",
        var title: String = "",
        var detail: String = "",
        var keys: String = "",
        var summary: String = ""
    )
}