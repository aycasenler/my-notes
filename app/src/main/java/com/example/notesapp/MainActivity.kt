package com.example.notesapp

import android.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.LayoutInflater
import kotlinx.android.synthetic.main.custom_dialog.view.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initLoginUserFragment()

    }

    override fun onBackPressed() {
        when (supportFragmentManager.backStackEntryCount) {
            1 -> {
                val mDialogView =
                    LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
                val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
                val mAlertDialog = mBuilder.show()
                mDialogView.dialog_txt?.text = "Are you sure ?"
                mDialogView.ok_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                    finish()
                }
                mDialogView.cancel_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                }
            }
            2 -> {
                val mDialogView =
                    LayoutInflater.from(this).inflate(R.layout.custom_dialog, null)
                val mBuilder = AlertDialog.Builder(this).setView(mDialogView)
                val mAlertDialog = mBuilder.show()

                mDialogView.dialog_txt?.text = "Are you sure you want to go back login screen ?"
                mDialogView.ok_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                    supportFragmentManager.popBackStack("LoginUserFragment", 2)

                }
                mDialogView.cancel_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                }
            }

            else -> {
                supportFragmentManager.popBackStack()
            }
        }
    }

    fun initLoginUserFragment() {
        val loginUserFragment = LoginUserFragment()
        val transaction = supportFragmentManager.beginTransaction()
        transaction.add(R.id.frameLayout, loginUserFragment, "LoginUserFragment")
        transaction.addToBackStack("LoginUserFragment")
        transaction.commit()
    }


}
