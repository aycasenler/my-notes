package com.example.notesapp

import android.content.Context
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.Typeface
import android.os.Build
import android.os.Bundle
import android.text.SpannableString
import android.text.TextUtils
import android.text.method.LinkMovementMethod
import android.text.style.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.CubeGrid
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class LoginUserFragment : Fragment(), View.OnClickListener {

    private var emailEt: EditText? = null
    private var passwordEt: EditText? = null
    private var loginUserBtn: Button? = null
    private var signUpUserTxt: TextView? = null
    lateinit var ref: DatabaseReference
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var progressBar: ProgressBar

    lateinit var forgotPassTxt: TextView
    lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.login_user_fragment, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        emailEt = view.findViewById(R.id.user_mail_et)
        passwordEt = view.findViewById(R.id.user_pass_et)
        loginUserBtn = view.findViewById(R.id.login_btn)
        signUpUserTxt = view.findViewById(R.id.sign_up_user_txt)
        forgotPassTxt = view.findViewById(R.id.forgot_pass_txt)
        progressBar = view.findViewById(R.id.progress)

        mAuth = FirebaseAuth.getInstance()

        preferences = activity!!.getSharedPreferences("userKEY", Context.MODE_PRIVATE)
        editor = preferences.edit()

        emailEt?.setText(preferences.getString("emailKEY", "").toString())
        passwordEt?.setText(preferences.getString("passwordKEY", "").toString())

        ref = FirebaseDatabase.getInstance().getReference("Users")

        loginUserBtn?.setOnClickListener(this)
        forgotPassTxt.setOnClickListener(this)
        signUpUserTxt?.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when (v?.id) {
            loginUserBtn?.id -> {

                loginUser()
                editor.putString("emailKEY", emailEt?.text.toString())
                editor.putString("passwordKEY", passwordEt?.text.toString())
                editor.apply()
                editor.commit()
            }
            forgotPassTxt.id -> {
                initForgotPasswordFragment()

            }
            signUpUserTxt?.id ->{
                initSignUpUserFragment()
            }
        }
    }


    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    private fun loginUser() {
        val email = emailEt?.text.toString().trim()
        val password = passwordEt?.text.toString().trim()

        if (!TextUtils.isEmpty(email) && !TextUtils.isEmpty(password)) {

            val cubeGrid : Sprite = CubeGrid()
            progressBar.setIndeterminateDrawableTiled(cubeGrid)
            progressBar.visibility = View.VISIBLE

        }

        mAuth.signInWithEmailAndPassword(email, password)
            .addOnCompleteListener(activity!!) { task ->

                progressBar.visibility = View.GONE

                if (task.isSuccessful) {
                    initListNoteVerticalFragment()

                } else {
                    Toast.makeText(context, "Sign In is failed! ", Toast.LENGTH_SHORT).show()
                }
            }
    }

    private fun initListNoteVerticalFragment() {
        val listNoteVerticalFragment = ListNotesVerticalFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, listNoteVerticalFragment, "ListNotesVerticalFragment")
        transaction?.addToBackStack("ListNotesVerticalFragment")
        transaction?.commit()
    }


    fun initSignUpUserFragment() {
        val signUpUserFragment = SignUpUserFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, signUpUserFragment, "SignUpUserFragment")
        transaction?.addToBackStack("SignUpUserFragment")
        transaction?.commit()
    }

    private fun initForgotPasswordFragment() {
        val forgotPasswordFragment = ForgotPasswordFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, forgotPasswordFragment, "ForgotPasswordFragment")
        transaction?.addToBackStack("ForgotPasswordFragment")
        transaction?.commit()
    }
}
