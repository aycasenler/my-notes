package com.example.notesapp

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.FoldingCube

class SplashScreenActivity : AppCompatActivity(){
    private val SPLASH_TIME_OUT : Long =3000
    lateinit var progressBar: ProgressBar
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        progressBar = findViewById(R.id.progress)

        val foldingCube : Sprite = FoldingCube()
        progressBar.setIndeterminateDrawableTiled(foldingCube)
        progressBar.visibility = View.VISIBLE

        Handler().postDelayed({

            startActivity(Intent(this,MainActivity::class.java))

            finish()
        }, SPLASH_TIME_OUT)
    }
}