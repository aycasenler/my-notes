package com.example.notesapp

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class NoteDetailFragment : Fragment(), View.OnClickListener {

    private lateinit var cancelDetailNoteBtn: ImageButton
    private lateinit var noteTitleTxt: TextView
    private lateinit var noteDetailTxt: TextView
    private lateinit var noteKeyTxt: TextView
    private lateinit var noteSummaryTxt: TextView
    private lateinit var updateNoteBtn: ImageButton
    private lateinit var keyListForUpdate: String
    private lateinit var noteId : String
    lateinit var ref: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.note_detail_fragment, container, false)

        initView(view)
        return view
    }

    @SuppressLint("SetTextI18n")
    fun initView(view: View) {
        cancelDetailNoteBtn = view.findViewById(R.id.cancel_note_btn)
        noteTitleTxt = view.findViewById(R.id.note_title_txt)
        noteDetailTxt = view.findViewById(R.id.note_detail_txt)
        noteKeyTxt = view.findViewById(R.id.note_keys_txt)
        noteSummaryTxt = view.findViewById(R.id.note_summary_txt)
        updateNoteBtn = view.findViewById(R.id.update_note_btn)

        ref = FirebaseDatabase.getInstance().getReference("Notes")

        noteTitleTxt.text = this.arguments?.getString("Title")
        noteDetailTxt.text = this.arguments?.getString("Detail")
        noteSummaryTxt.text = this.arguments?.getString("Summary")
        noteId = this.arguments?.getString("NoteId").toString()
        keyListForUpdate = this.arguments?.getString("Key").toString()
        val keyList = this.arguments?.getString("Key")!!.split(",")
        var keys = ""
        for (h in 0..keyList.size - 1) {
            keys += " ${keyList.get(h)}\n"
        }
        noteKeyTxt.text = keys

        cancelDetailNoteBtn.setOnClickListener(this)
        updateNoteBtn.setOnClickListener(this)

    }

    override fun onClick(v: View?) {
        when (v?.id) {
            cancelDetailNoteBtn.id -> {
                activity!!.supportFragmentManager.popBackStack()
            }
            updateNoteBtn.id -> {

                initUpdateNoteFragment()

            }
        }
    }

    fun initUpdateNoteFragment() {
        val updateNoteFragment = UpdateNoteFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, updateNoteFragment, "UpdateNoteFragment")
        transaction?.addToBackStack("UpdateNoteFragment")
        val bundle = Bundle()

        bundle.putString("Title", noteTitleTxt.text.toString())
        bundle.putString("Detail", noteDetailTxt.text.toString())
        bundle.putString("Summary", noteSummaryTxt.text.toString())
        bundle.putString("Key", keyListForUpdate)
        bundle.putString("NoteId", noteId)
        updateNoteFragment.arguments = bundle
        transaction?.commit()
    }
}