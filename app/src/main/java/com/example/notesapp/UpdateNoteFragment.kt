package com.example.notesapp

import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class UpdateNoteFragment : Fragment(), View.OnClickListener {

    lateinit var noteTitleEt: EditText
    lateinit var noteDetailEt: EditText
    lateinit var noteKeyEt: EditText
    lateinit var noteSummaryEt: EditText
    lateinit var noteId: String
    lateinit var updateNoteBtn: Button
    lateinit var cancelUpdateNoteBtn: Button

    lateinit var mAuth: FirebaseAuth
    lateinit var ref: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.update_note_fragment, container, false)
        initView(view)
        return view
    }

    private fun initView(view: View) {

        noteDetailEt = view.findViewById(R.id.note_detail_et)
        noteTitleEt = view.findViewById(R.id.note_title_et)
        noteKeyEt = view.findViewById(R.id.note_keys_et)
        noteSummaryEt = view.findViewById(R.id.note_summary_et)
        updateNoteBtn = view.findViewById(R.id.update_note_btn)
        cancelUpdateNoteBtn = view.findViewById(R.id.cancel_update_note_btn)

        noteTitleEt.setText(this.arguments?.getString("Title"))
        noteDetailEt.setText(this.arguments?.getString("Detail"))
        noteKeyEt.setText(this.arguments?.getString("Key"))
        noteSummaryEt.setText(this.arguments?.getString("Summary"))
        noteId = this.arguments?.getString("NoteId").toString()

        mAuth = FirebaseAuth.getInstance()
        ref = FirebaseDatabase.getInstance().getReference("Notes")

        updateNoteBtn.setOnClickListener(this)
        cancelUpdateNoteBtn.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            updateNoteBtn.id -> {
                updateNote()
                Handler().postDelayed({
                    initListNoteVerticalFragment()
                }, 1000)

            }
            cancelUpdateNoteBtn.id -> {
                initListNoteVerticalFragment()
            }
        }
    }

    private fun updateNote() {
        val title = noteTitleEt.text.toString().trim()
        if (title.isEmpty()) {
            noteTitleEt.error = "Please enter note title! "
            return
        }
        val detail = noteDetailEt.text.toString().trim()
        if (detail.isEmpty()) {
            noteDetailEt.error = "Please enter note detail! "
            return
        }
        var userId: String? = null
        if (mAuth.currentUser !== null) {
            userId = mAuth.currentUser?.uid
        }
        val keys = noteKeyEt.text.toString().trim()
        val summary = noteSummaryEt.text.toString().trim()
        val noteId = noteId
        val notes = AddNewNoteFragment.Unotes(noteId, userId!!, title, detail, keys, summary)

        ref.child(noteId).setValue(notes).addOnCompleteListener {
            Toast.makeText(context, "Note update successfully!", Toast.LENGTH_SHORT).show()
        }
    }

    fun initListNoteVerticalFragment() {
        val listNoteVerticalFragment = ListNotesVerticalFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, listNoteVerticalFragment, "ListNotesVerticalFragment")
        transaction?.addToBackStack("ListNotesVerticalFragment")
        transaction?.commit()
    }
}