package com.example.notesapp

import android.os.Bundle
import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.google.firebase.auth.FirebaseAuth

class ForgotPasswordFragment :Fragment(){
    lateinit var etForgotPass : EditText
    lateinit var btnForgotPass : Button

    lateinit var mAuth: FirebaseAuth

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.forgot_password_fragment, container, false)
        initView(view)
        return view
    }

    fun initView(view : View){
        etForgotPass = view.findViewById(R.id.reset_password_et)
        btnForgotPass = view.findViewById(R.id.reset_password_btn)

        mAuth = FirebaseAuth.getInstance()
        btnForgotPass.setOnClickListener { sendPasswordResetEmail() }

    }


    private fun sendPasswordResetEmail() {
        val email = etForgotPass.text.toString()
        if (!TextUtils.isEmpty(email)) {
            mAuth
                .sendPasswordResetEmail(email)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {


                        Toast.makeText(context, "Email sent.", Toast.LENGTH_SHORT).show()
                        activity!!.supportFragmentManager.popBackStack()
                    } else {

                        Toast.makeText(context, "No user found with this email.", Toast.LENGTH_SHORT).show()
                    }
                }
        } else {
            Toast.makeText(context, "Enter Email", Toast.LENGTH_SHORT).show()
        }
    }

}
