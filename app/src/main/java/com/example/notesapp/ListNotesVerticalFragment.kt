package com.example.notesapp

import android.app.AlertDialog
import android.graphics.*
import android.os.Bundle
import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.ItemTouchHelper.*
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.custom_dialog.view.*

lateinit var userId: String

class ListNotesVerticalFragment : Fragment(), View.OnClickListener {
    private var btnVerticalToGrid: ImageButton? = null
    private val p = Paint()
    private var btnAdd: ImageButton? = null
    lateinit var recyclerView: RecyclerView
    lateinit var btnExitPage: ImageButton
    lateinit var mAuth: FirebaseAuth
    lateinit var ref: DatabaseReference
    lateinit var noteList: MutableList<AddNewNoteFragment.Unotes>

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.list_notes_vertical_fragment, container, false)
        initView(view)
        return view
    }

    fun initView(view: View) {
        btnAdd = view.findViewById(R.id.btn_add)
        btnVerticalToGrid = view.findViewById(R.id.btn_grid)
        btnExitPage = view.findViewById(R.id.btn_exit)
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = LinearLayoutManager(
            context,
            RecyclerView.VERTICAL,
            false
        ) as RecyclerView.LayoutManager?
        noteList = mutableListOf()

        userId = this.arguments?.getString("UserId").toString()
        mAuth = FirebaseAuth.getInstance()
        ref = FirebaseDatabase.getInstance().getReference("Notes")

        btnVerticalToGrid?.setOnClickListener(this)
        btnAdd?.setOnClickListener(this)
        btnExitPage.setOnClickListener(this)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(context, "Error Occurred " + p0.toException(), Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onDataChange(p0: DataSnapshot) {
                noteList.clear()
                if (p0.exists()) {
                    for (h in p0.children) {
                        val note = h.getValue(AddNewNoteFragment.Unotes::class.java)
                        if (mAuth.currentUser?.uid == note?.userId) {
                            noteList.add(note!!)
                        }
                    }

                    val adapter = ListNotesVerticalAdapter(noteList, context!!)
                    recyclerView.adapter = adapter
                    val simpleItemTouchCallback =
                        object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT or RIGHT) {
                            override fun onMove(
                                recyclerView: RecyclerView,
                                viewHolder: RecyclerView.ViewHolder,
                                target: RecyclerView.ViewHolder
                            ): Boolean {
                                return false
                            }

                            override fun onSwiped(
                                viewHolder: RecyclerView.ViewHolder,
                                direction: Int
                            ) {
                                val position = viewHolder.adapterPosition

                                when (direction) {

                                    ItemTouchHelper.LEFT -> {
                                        ref.child(noteList.get(position).noteId).removeValue()
                                        adapter.removeItem(position)
                                    }


                                    ItemTouchHelper.RIGHT -> {

                                        val bundle = Bundle()

                                        bundle.putString("Title", noteList.get(position).title)
                                        bundle.putString("Detail", noteList.get(position).detail)
                                        bundle.putString("Key", noteList.get(position).keys)
                                        bundle.putString("Summary", noteList.get(position).summary)
                                        bundle.putString("UserId", noteList.get(position).userId)
                                        bundle.putString("NoteId", noteList.get(position).noteId)

                                        val updateNoteFragment = UpdateNoteFragment()
                                        updateNoteFragment.arguments = bundle
                                        val transaction =
                                            activity?.supportFragmentManager?.beginTransaction()
                                        transaction?.add(
                                            R.id.frameLayout,
                                            updateNoteFragment,
                                            "UpdateNoteFragment"
                                        )
                                        transaction?.addToBackStack("UpdateNoteFragment")
                                        transaction?.commit()
                                    }

                                    else -> {
                                        val deletedModel = noteList[position]
                                        adapter.removeItem(position)

                                    }
                                }
                            }

                            override fun onChildDraw(
                                c: Canvas,
                                recyclerView: RecyclerView,
                                viewHolder: RecyclerView.ViewHolder,
                                dX: Float,
                                dY: Float,
                                actionState: Int,
                                isCurrentlyActive: Boolean
                            ) {

                                var icon: Bitmap
                                if (actionState == ACTION_STATE_SWIPE) {

                                    val itemView = viewHolder.itemView
                                    val height = itemView.bottom.toFloat() - itemView.top.toFloat()
                                    val width = height / 3

                                    if (dX > 0) {

                                        p.color = Color.parseColor("#388E3C")
                                        val background =
                                            RectF(
                                                itemView.left.toFloat(),
                                                itemView.top.toFloat(),
                                                dX,
                                                itemView.bottom.toFloat()
                                            )
                                        c.drawRect(background, p)
                                        icon = BitmapFactory.decodeResource(
                                            resources,
                                            R.drawable.update_ic
                                        )
                                        val icon_dest = RectF(
                                            itemView.left.toFloat() + width,
                                            itemView.top.toFloat() + width,
                                            itemView.left.toFloat() + 2 * width,
                                            itemView.bottom.toFloat() - width
                                        )
                                        c.drawBitmap(icon, null, icon_dest, p)
                                        Handler().postDelayed({
                                            adapter.notifyDataSetChanged()
                                        }, 500)

                                    }
                                    if(dX < 0 )  {
                                        p.color = Color.parseColor("#D32F2F")
                                        val background = RectF(
                                            itemView.right.toFloat() + dX,
                                            itemView.top.toFloat(),
                                            itemView.right.toFloat(),
                                            itemView.bottom.toFloat()
                                        )
                                        c.drawRect(background, p)
                                        icon = BitmapFactory.decodeResource(
                                            resources,
                                            R.drawable.delete_ic
                                        )
                                        val icon_dest = RectF(
                                            itemView.right.toFloat() - 2 * width,
                                            itemView.top.toFloat() + width,
                                            itemView.right.toFloat() - width,
                                            itemView.bottom.toFloat() - width
                                        )
                                        c.drawBitmap(icon, null, icon_dest, p)

                                    }
                                }
                                super.onChildDraw(
                                    c,
                                    recyclerView,
                                    viewHolder,
                                    dX,
                                    dY,
                                    actionState,
                                    isCurrentlyActive
                                )
                            }
                        }

                    val itemTouchHelper = ItemTouchHelper(simpleItemTouchCallback)
                    itemTouchHelper.attachToRecyclerView(recyclerView)

                }

            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnAdd?.id -> {
                initAddNewNoteFragment()

            }
            btnVerticalToGrid?.id -> {
                initListNoteGridFragment()
            }
            btnExitPage.id -> {

                val mDialogView =
                    LayoutInflater.from(context).inflate(R.layout.custom_dialog, null)
                val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
                val mAlertDialog = mBuilder.show()

                mDialogView.dialog_txt?.text = "Are you sure you want to logout ?"
                mDialogView.ok_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                    mAuth.signOut()
                    activity!!.supportFragmentManager.popBackStack("LoginUserFragment", 2)
                }
                mDialogView.cancel_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                }
            }
        }
    }

    fun initListNoteGridFragment() {
        val listNoteGridFragment = ListNotesGridFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, listNoteGridFragment, "ListNotesGridFragment")
        transaction?.addToBackStack("ListNotesGridFragment")
        transaction?.commit()
    }

    fun initAddNewNoteFragment() {
        val addNewNoteFragment = AddNewNoteFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, addNewNoteFragment, "AddNewNoteFragment")
        transaction?.addToBackStack("AddNewNoteFragment")
        transaction?.commit()
    }

}
