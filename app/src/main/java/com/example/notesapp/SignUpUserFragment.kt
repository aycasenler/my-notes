package com.example.notesapp

import android.app.ProgressDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import androidx.fragment.app.Fragment
import com.github.ybq.android.spinkit.sprite.Sprite
import com.github.ybq.android.spinkit.style.CubeGrid
import com.github.ybq.android.spinkit.style.WanderingCubes
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.DatabaseReference
import com.google.firebase.database.FirebaseDatabase

class SignUpUserFragment : Fragment(), View.OnClickListener {

    lateinit var etEmail: EditText
    lateinit var etPassword: EditText
    lateinit var btnSignUp: Button

    lateinit var mDatabaseReference: DatabaseReference
    lateinit var mDatabase: FirebaseDatabase
    lateinit var mAuth: FirebaseAuth
    lateinit var mProgressBar: ProgressDialog
    lateinit var progressBar: ProgressBar


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.signup_user_fragment, container, false)
        initView(view)
        return view
    }

    fun initView(view: View) {
        etEmail = view.findViewById(R.id.edtMail)
        etPassword = view.findViewById(R.id.edtPass)
        btnSignUp = view.findViewById(R.id.btnSignUp)
        mProgressBar = ProgressDialog(context)
        progressBar = view.findViewById(R.id.progress)

        mDatabase = FirebaseDatabase.getInstance()
        mDatabaseReference = mDatabase!!.reference!!.child("Users")
        mAuth = FirebaseAuth.getInstance()

        btnSignUp.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnSignUp.id -> {
                createNewAccount()
            }
        }
    }

    private fun createNewAccount() {
        val email = etEmail.text.toString()
        val password = etPassword.text.toString()
        if (email.isEmpty()) {
            etEmail.error = "Please enter mail! "
        }
        if (password.isEmpty()) {
            etPassword.error = "Please enter password! "
        }

        val wanderingCubes : Sprite = WanderingCubes()
        progressBar.setIndeterminateDrawableTiled(wanderingCubes)
        progressBar.visibility = View.VISIBLE

        mAuth.createUserWithEmailAndPassword(email, password)
            .addOnCompleteListener(activity!!) { task ->
                progressBar.visibility = View.GONE

                if (task.isSuccessful) {

                    Toast.makeText(context, "User add successfully!", Toast.LENGTH_SHORT).show()
                    val userId = mAuth.currentUser!!.uid

                    verifyEmail()

                    val currentUserDb = mDatabaseReference.child(userId)
                    currentUserDb.child("uemail").setValue(email)
                    currentUserDb.child("upassword").setValue(password)
                } else {

                    Toast.makeText(
                        context,
                        "Failed to add user! $task.exception",
                        Toast.LENGTH_SHORT
                    ).show()
                    Toast.makeText(
                        context, "Authentication failed.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            }

    }

    private fun verifyEmail() {
        val mUser = mAuth.currentUser
        mUser?.sendEmailVerification()?.addOnCompleteListener(activity!!) { task ->
            if (task.isSuccessful) {
                Toast.makeText(
                    context,
                    "Verification email sent to " + mUser.email,
                    Toast.LENGTH_SHORT
                ).show()
                activity!!.supportFragmentManager.popBackStack("LoginUserFragment", 2)
            } else {
                Toast.makeText(
                    context,
                    "Failed to send verification email.",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
    }


}


