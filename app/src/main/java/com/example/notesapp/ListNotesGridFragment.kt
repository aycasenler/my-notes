package com.example.notesapp

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.custom_dialog.view.*


class ListNotesGridFragment : Fragment(), View.OnClickListener {

    lateinit var noteList: MutableList<AddNewNoteFragment.Unotes>
    private var btnGridToVertical: ImageButton? = null
    private var btnAdd: ImageButton? = null
    lateinit var btnExitPage: ImageButton
    lateinit var recyclerView: RecyclerView
    lateinit var mAuth: FirebaseAuth
    lateinit var ref: DatabaseReference

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.list_notes_grid_fragment, container, false)
        init(view)
        return view
    }

    fun init(view: View) {
        btnAdd = view.findViewById(R.id.btn_add)
        btnGridToVertical = view.findViewById(R.id.btn_grid)
        btnExitPage = view.findViewById(R.id.btn_exit)
        noteList = mutableListOf()
        recyclerView = view.findViewById(R.id.recyclerView)
        recyclerView.layoutManager = GridLayoutManager(context, 2)



        ref = FirebaseDatabase.getInstance().getReference("Notes")
        mAuth = FirebaseAuth.getInstance()

        btnGridToVertical?.setOnClickListener(this)
        btnAdd?.setOnClickListener(this)
        btnExitPage.setOnClickListener(this)

        ref.addValueEventListener(object : ValueEventListener {
            override fun onCancelled(p0: DatabaseError) {
                Toast.makeText(context, "Error Occurred " + p0.toException(), Toast.LENGTH_SHORT)
                    .show()
            }

            override fun onDataChange(p0: DataSnapshot) {
                noteList.clear()
                if (p0.exists()) {
                    for (h in p0.children) {
                        val note = h.getValue(AddNewNoteFragment.Unotes::class.java)
                        if(mAuth.currentUser?.uid == note?.userId){
                            noteList.add(note!!)
                        }
                    }

                    val adapter = ListNotesGridAdapter(noteList, context!!)
                    recyclerView.adapter = adapter
                }
            }
        })
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            btnAdd?.id -> {
                initAddNewNoteFragment()
            }
            btnGridToVertical?.id -> {
                initListNoteVerticalFragment()
            }
            btnExitPage.id -> {
                val mDialogView =
                    LayoutInflater.from(context).inflate(R.layout.custom_dialog, null)
                val mBuilder = AlertDialog.Builder(context).setView(mDialogView)
                val mAlertDialog = mBuilder.show()

                mDialogView.dialog_txt?.text = "Are you sure you want to logout ?"
                mDialogView.ok_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                    mAuth.signOut()
                    activity!!.supportFragmentManager.popBackStack("LoginUserFragment", 2)
                }
                mDialogView.cancel_btn.setOnClickListener {
                    mAlertDialog.dismiss()
                }

            }
        }
    }

    fun initListNoteVerticalFragment() {
        val listNoteVerticalFragment = ListNotesVerticalFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, listNoteVerticalFragment, "ListNotesVerticalFragment")
        transaction?.addToBackStack("ListNotesVerticalFragment")
        transaction?.commit()
    }

    fun initAddNewNoteFragment() {
        val addNewNoteFragment = AddNewNoteFragment()
        val transaction = activity?.supportFragmentManager?.beginTransaction()
        transaction?.add(R.id.frameLayout, addNewNoteFragment, "AddNewNoteFragment")
        transaction?.addToBackStack("AddNewNoteFragment")
        transaction?.commit()
    }
}