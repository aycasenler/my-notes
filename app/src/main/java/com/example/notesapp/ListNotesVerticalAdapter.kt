package com.example.notesapp

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth

class ListNotesVerticalAdapter(
    val noteList: MutableList<AddNewNoteFragment.Unotes>,
    val context: Context
) :
    RecyclerView.Adapter<ListNotesVerticalAdapter.MyViewHolder>() {
    val onItemClick: ((AddNewNoteFragment.Unotes) -> Unit)? = null
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = LayoutInflater.from(parent.context)
            .inflate(R.layout.list_notes_vertical_item, parent, false)
        return MyViewHolder(itemView)
    }

    override fun getItemCount(): Int {
        return noteList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val note = noteList[position]

        var mAuth = FirebaseAuth.getInstance()
        for (h in noteList) {
            if (note.userId == mAuth.currentUser?.uid) {
                holder.txtTitle?.text = note.title
                holder.txtDetail?.text = note.detail
                val keyList = note.keys.split(",")
                if (keyList.size == 1 ) {
                    holder.txtKey?.visibility = View.VISIBLE
                    holder.txtKey?.text = keyList.get(0)
                }
               else if (keyList.size == 2 ) {
                    holder.txtKey?.visibility = View.VISIBLE
                    holder.txtKey?.text = keyList.get(0)
                    holder.txtKey2?.visibility = View.VISIBLE
                    holder.txtKey2?.text = keyList.get(1)
                }
               else if (keyList.size >= 3 ) {
                    holder.txtKey?.visibility = View.VISIBLE
                    holder.txtKey?.text = keyList.get(0)
                    holder.txtKey2?.visibility = View.VISIBLE
                    holder.txtKey2?.text = keyList.get(1)
                    holder.txtKey3?.visibility = View.VISIBLE
                    holder.txtKey3?.text = keyList.get(2)
                }
            }
        }

        holder.itemView.setOnClickListener {
            clickNote(note)
        }
    }

    private fun clickNote(note: AddNewNoteFragment.Unotes) {
        val position = noteList.indexOf(note)
        val bundle = Bundle()

        bundle.putString("Title", noteList.get(position).title)
        bundle.putString("Detail", noteList.get(position).detail)
        bundle.putString("Key", noteList.get(position).keys)
        bundle.putString("Summary", noteList.get(position).summary)
        bundle.putString("UserId", noteList.get(position).userId)
        bundle.putString("NoteId", noteList.get(position).noteId)

        val detailsFragment = NoteDetailFragment()
        detailsFragment.arguments = bundle

        val transaction =
            (context as FragmentActivity).getSupportFragmentManager().beginTransaction()
        transaction.add(R.id.frameLayout, detailsFragment, "DetailsFragment")
        transaction.addToBackStack("DetailsFragment")
        transaction.commit()
    }

    fun removeItem(position: Int) {
        noteList.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val txtTitle: TextView? = itemView.findViewById(R.id.txtTitle)
        val txtDetail: TextView? = itemView.findViewById(R.id.txtDetail)
        val txtKey: TextView? = itemView.findViewById(R.id.txtKey)
        val txtKey2: TextView? = itemView.findViewById(R.id.txtKey2)
        val txtKey3: TextView? = itemView.findViewById(R.id.txtKey3)

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(noteList[adapterPosition])
            }
        }
    }
}